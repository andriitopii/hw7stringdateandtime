// 1. Як можна створити рядок у JavaScript?
// ! Рядок в Java script створити кількома методами;
// ! let a = "String" також можна використовувати
//* let a ="Подвійні лапки";
//* let b ='Одинарні лапки';
//* let c =`Зворотні лапки (Шаблонні)`;
// *let n = new String('Привіт') // тут буде обєкт який можна буде методами конвертувати в рядок. Наприклад методом toString()
//* console.log(a);
//* console.log(b);
//* console.log(c);
//*console.log(n.toString());

//! 2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
//* Різниці між '' і "" немає. Просто рекомендується вибирати один стиль лапок. А от `` дозволяє додавати динамічні дані типу змінних прямо між зворотніх лапок.
//! 3. Як перевірити, чи два рядки рівні між собою?
//* Порівнювати між собою рядки можна за допомогою оператора == або ===.
// 4. Що повертає Date.now()?
//* console.log(Date.now());  повертає кількість мілісекунд які пройшли від 1 січня 1970 року
//! 5. Чим відрізняється Date.now() від new Date()?
//* let a = new Date(); a -  це об'єкт який повертає Tue Jan 23 2024 11:37:09 GMT+0100. Тобто створюється обєкт.
//* let b = Date.now(); b - отримує значеня типу number без створення обєкту
//* console.log(a);
//* console.log(b);
// Практичні завдання
// 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
// (читається однаково зліва направо і справа наліво), або false в іншому випадку.

// let strCheck = "TENET";
// const isPalindrome = (str) => {
//     let reverse = new String(str);
//     reverse = reverse.split('').reverse((a,b)=>{a-b}).join().replaceAll(",","");
//     if(reverse === str) {return true} else {return false};
// }
// console.log(isPalindrome(strCheck));

// 2. Створіть функцію, яка перевіряє довжину рядка.
// Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший.
// Ця функція стане в нагоді для валідації форми. Приклади використання функції:
// // Рядок коротше 20 символів
// funcName('checked string', 20); // true
// // Довжина рядка дорівнює 18 символів
// funcName('checked string', 10); // false

// let strCheck2 =  "GORDONNNNNsdfsdfsdfsdfsdfsdf";

// const fnLenght = (str, lghNum) => {
//    if(str.length <= lghNum){
//     return true;
//    }else{
//     return false;
//    }

// }
// console.log(fnLenght(strCheck2, 10));

// 3. Створіть функцію, яка визначає скільки повних років користувачу.
// Отримайте дату народження користувача через prompt.
// Функція повина повертати значення повних років на дату виклику функцію.
let userBirthDay;
let nowDate = new Date();
do {
  userBirthDay = prompt(
    "Введіть дату народжееея в форматті Рік/Місяць/День. Через пробіл."
  );
  userBirthDay = new String(userBirthDay);
} while (userBirthDay === "" || userBirthDay === null || userBirthDay.length != 8 || isNaN(userBirthDay));



function returnFullYears(objUser, objDate) {
    let year = Number(objUser.slice(0,4));
    let day = Number(objUser.slice(6));
    let mounth = Number(objUser.slice(4,6)) - 1;
    let dataUser = new Date(year, mounth, day);
if(year < 1900 || day > 31 || mounth > 11){
    return "ERROR"
}else{
return(Math.floor((objDate - dataUser) / (1000 * 60 * 60 * 24 * 30 * 12)));
}

}

console.log("Повних років " + returnFullYears(userBirthDay, nowDate));
